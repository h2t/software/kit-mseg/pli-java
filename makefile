deb_dist = deb-dist

.depsdeb:
	@echo ant
	@echo ivy
	@echo openjdk-7-jdk
	@echo zeroc-ice35

.bashrc:
	@printf "# <PLI-JAVA>\n"
	@printf "export MSEG_PLI_JAVA_DIR=\"$(realpath $(dir $(lastword $(MAKEFILE_LIST))))\"\n"
	@printf "export CLASSPATH=\"\$${CLASSPATH}:\$${MSEG_PLI_JAVA_DIR}/build/lib/plijava.jar\"\n"
	@printf "# </PLI-JAVA>\n\n"

clean:
	@rm -r ./build 2> /dev/null || true
	@rm -r ./${deb_dist} 2> /dev/null || true

install:
	ant -lib "/usr/share/java/ivy.jar" -lib "./build/lib/jdeb.jar" plijava-jar

package: install
	@mkdir ./${deb_dist} 2> /dev/null || true
	ant -lib "/usr/share/java/ivy.jar" -lib "./build/lib/jdeb.jar" plijava-package
	@mv ./build/*.deb ./${deb_dist}/
